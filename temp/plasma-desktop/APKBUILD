# Forked from Alpine to build Plasma 5.17 pre-release
pkgname=plasma-desktop
pkgver=5.16.90
pkgrel=0
pkgdesc="KDE Plasma Desktop"
arch="all !ppc64le !s390x !armhf" # Limited by plasma-workspace -> libksysguard -> qt5-qtwebengine
url='https://www.kde.org/workspaces/plasmadesktop/'
license="GPL-2.0-only AND LGPL-2.1-only"
depends="kirigami2 plasma-workspace setxkbmap qqc2-desktop-style"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	qt5-qtsvg-dev
	kauth-dev
	plasma-framework-dev
	ki18n-dev
	kcmutils-dev
	knewstuff-dev
	kdelibs4support-dev
	knotifications-dev
	knotifyconfig-dev
	attica-dev
	kwallet-dev
	krunner-dev
	kglobalaccel-dev
	kdeclarative-dev
	kpeople-dev
	kdbusaddons-dev
	kactivities-dev
	kactivities-stats-dev
	kconfig-dev
	plasma-workspace-dev
	kwin-dev
	kitemmodels-dev
	kemoticons-dev
	baloo-dev
	fontconfig-dev
	eudev-dev
	xf86-input-libinput-dev
	xf86-input-evdev-dev 
	xf86-input-synaptics-dev
	libxkbfile-dev
	libxcursor-dev
	libxi-dev
	"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev"
checkdepends="xvfb-run iso-codes"
source="https://download.kde.org/unstable/plasma/$pkgver/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang knetattach"
options="!check" # Requires running dbus

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" make install

	rm "$pkgdir"/usr/bin/knetattach
	rm "$pkgdir"/usr/share/applications/org.kde.knetattach.desktop
}

knetattach() {
	pkgdesc="Wizard which makes it easier to integrate network resources with the Plasma Desktop"
	depends="kdelibs4support"

	cd "$builddir"/knetattach
	DESTDIR="$subpkgdir" make install
}
sha512sums="26a8d0f85c264ff03d0a36f59895154bc0f067b44f79c3f4d6809b14b86c8529aaa2f0b93cb6fbb4e32b45acdaf97203cdebf8033e244b1ce515697eb7dd2928  plasma-desktop-5.16.90.tar.xz"
